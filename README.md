# Use-Case Personenzähler

## Projektbeschreibung

In diesem Use-Case wurde die Integration eines [LoRaWAN-Personenzählers](https://imbuildings.com/products/people-counter-lorawan/) von IM-Buildings durchgeführt. Der Sensor zählt die Anzahl der Personen in 15-Minuten-Abschnitten. Dabei wird zwischen eingehenden und ausgehenden Personen unterschieden.

Der Use-Case wurde auf der FIWARE-basierten [Urban Dataspace Platform](https://gitlab.com/urban-dataspace-platform/core-platform) entwickelt und integriert.

## Installationsanleitung

Um den Anwendungsfall zu installieren sind folgende Schritte nötig:  
1. Eine lokale Maschine für die Installation ist vorzubereiten (Siehe https://gitlab.com/urban-dataspace-platform/core-platform/-/blob/master/00_documents/Admin-guides/cluster.md).
2. Eine NodeRED-Instanz deployen oder eine vorhandene Instanz verwenden:
    - Deployment analog zu den *demo_usecases*: https://gitlab.com/urban-dataspace-platform/use_cases/demo_usecases/deployment
3. *flow.json* in der NodeRED-Instanz deployen.

## Gebrauchsanweisung

Der NodeRED-Flow ruft die Daten per HTTP-Request von der ElementIOT-API ab. Dabei kann eine Folder-ID angegeben werden, um alle Devices aus einem Ordner mit einem Request abzufragen. Für die ElementIOT-API ist ein API-Key erforderlich.

Diese Daten werden in das NGSI-kompatible [CrowdFlowObserved Smart-Data-Model](https://github.com/smart-data-models/dataModel.Transportation/blob/master/CrowdFlowObserved/README.md) umgewandelt und an die Datenplattform gesendet:

```
{
   "id":"urn:nsgi-ld:CrowdFlowObserved:f82bb502-09d1-464e-93d7-4dd462aa938b",
   "type":"CrowdFlowObserved",
   "dateObserved":{
      "type":"DateTime",
      "value":"2024-05-03T11:59:05.491Z"
   },
   "dateObservedFrom":{
      "type":"DateTime",
      "value":"2024-05-03T11:44:05.491Z"
   },
   "dateObservedTo":{
      "type":"DateTime",
      "value":"2024-05-03T11:59:05.491Z"
   },
   "name":{
      "type":"Text",
      "value":"A5235"
   },
   "direction":{
      "type":"Text",
      "value":"inbound"
   },
   "peopleCount":{
      "type":"Number",
      "value":7
   },
   "totalPeopleCount":{
      "type":"Number",
      "value":2314
   }
}
```
Dabei muss beachtet werden, dass jeder Sensor 2 Werte für einen Zeitraum liefert; Inbound- und Outbound-Zählungen, welche im Datenmodell mit dem *direction*-Attribut unterschieden werden.

## Kontaktinformationen

Dieses Projekt wurde von der [Hypertegrity AG](https://www.hypertegrity.de/) entwickelt.

## Lizenz

Dieses Projekt wird unter der EUPL 1.2 veröffentlicht.

## Link zum Original-Repository

https://gitlab.com/urban-dataspace-platform/use_cases/integration/people_counter/nr-people-counter